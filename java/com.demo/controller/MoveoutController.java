package com.southwind.controller;

import com.southwind.entity.Moveout;
import com.southwind.service.MoveoutService;
import com.southwind.service.StudentServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/moveout")
public class MoveoutController {

    //注入方法
    @Autowired
    private StudentServie studentServie;
    @Autowired
    private MoveoutService moveoutService;

    @GetMapping("/list")
    public ModelAndView list(){
        //创建对象，设定name
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("moveoutregister");
        //调用
        modelAndView.addObject("list", this.studentServie.moveoutList());
        return modelAndView;
    }

    //搜索功能
    @PostMapping("/search")
    public ModelAndView search(String key,String value){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("moveoutregister");
        modelAndView.addObject("list", this.studentServie.searchForMoveoutList(key, value));
        return modelAndView;
    }

    @PostMapping("/register")
    public String register(Moveout moveout){
        this.studentServie.moveout(moveout);
        return "redirect:/moveout/list";
    }

    @GetMapping("/record")
    public ModelAndView record(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("moveoutrecord");
        modelAndView.addObject("list", this.moveoutService.list());
        return modelAndView;
    }


    @PostMapping("/recordSearch")
    public ModelAndView recordSearch(String key,String value){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("moveoutrecord");
        modelAndView.addObject("list", this.moveoutService.search(key, value));
        return modelAndView;
    }
}
