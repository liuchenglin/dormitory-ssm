package com.southwind.mapper;

import com.southwind.entity.Moveout;
import com.southwind.entity.Student;

import java.util.List;

public interface StudentMapper {
    /**
     * 查询所有学生信息，返回学生列表
     */
    public List<Student> list();

    /**
     * 根据学号查询学生信息，返回符合条件的学生列表
     * @param value 学号
     */
    public List<Student> searchByNumber(String value);

    /**
     * 根据姓名查询学生信息，返回符合条件的学生列表
     * @param value 姓名
     */
    public List<Student> searchByName(String value);

    /**
     * 保存学生信息
     * @param student 学生对象
     */
    public void save(Student student);

    /**
     * 更新学生信息
     * @param student 学生对象
     */
    public void update(Student student);

    /**
     * 根据ID删除学生信息
     * @param id 学生ID
     */
    public void delete(Integer id);

    /**
     * 根据宿舍ID查询学生ID，返回学生ID列表
     * @param dormitoryId 宿舍ID
     */
    public List<Integer> findStudentIdByDormitoryId(Integer dormitoryId);

    /**
     * 将学生所在宿舍ID重置为指定的宿舍ID
     * @param studentId 学生ID
     * @param dormitoryId 宿舍ID
     */
    public void resetDormitoryId(Integer studentId,Integer dormitoryId);

    /**
     * 查询所有需要退宿的学生信息，返回学生列表
     */
    public List<Student> moveoutList();

    /**
     * 根据学号查询需要退宿的学生信息，返回符合条件的学生列表
     * @param value 学号
     */
    public List<Student> searchForMoveoutByNumber(String value);

    /**
     * 根据姓名查询需要退宿的学生信息，返回符合条件的学生列表
     * @param value 姓名
     */
    public List<Student> searchForMoveoutByName(String value);

    /**
     * 将指定ID的学生状态设置为已退宿
     * @param id 学生ID
     */
    public void updateStateById(Integer id);

    /**
     * 将指定ID的学生退宿，并记录退宿时间等信息
     * @param moveout 退宿信息对象
     */
    public void moveout(Moveout moveout);

    /**
     * 根据宿舍ID查询学生信息，返回符合条件的学生列表
     * @param dormitoryId 宿舍ID
     */
    public List<Student> findByDormitoryId(Integer dormitoryId);
}
