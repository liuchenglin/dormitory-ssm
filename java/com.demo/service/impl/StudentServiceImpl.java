package com.southwind.service.impl;

import com.southwind.entity.Moveout;
import com.southwind.entity.Student;
import com.southwind.mapper.DormitoryMapper;
import com.southwind.mapper.StudentMapper;
import com.southwind.service.StudentServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentServie {

    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private DormitoryMapper dormitoryMapper;

    @Override
    public List<Student> list() {
        return this.studentMapper.list();
    }

    @Override
    public List<Student> search(String key, String value) {
        // 如果value为空，则返回全部学生信息
        if(value.equals("")) return this.studentMapper.list();
        List<Student> list = null;
        switch (key){
            case "number":
                list = this.studentMapper.searchByNumber(value);
                break;
            case "name":
                list = this.studentMapper.searchByName(value);
                break;
        }
        return list;
    }

    @Override
    public void save(Student student) {
        // 获取当前时间，并将其格式化为“yyyy-MM-dd”形式
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        student.setCreateDate(format.format(date));
        try {
            // 将学生信息插入到数据库中
            this.studentMapper.save(student);
            // 将该学生所属的宿舍的可用床位数减一
            this.dormitoryMapper.subAvailable(student.getDormitoryId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Student student) {
        try {
            // 更新学生信息
            this.studentMapper.update(student);
            // 如果学生更换了宿舍，则将原宿舍的可用床位数加一，新宿舍的可用床位数减一
            if(!student.getDormitoryId().equals(student.getOldDormitoryId())){
                this.dormitoryMapper.subAvailable(student.getDormitoryId());
                this.dormitoryMapper.addAvailable(student.getOldDormitoryId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Student student) {
        try {
            // 删除学生信息
            this.studentMapper.delete(student.getId());
            // 将该学生所属的宿舍的可用床位数加一
            this.dormitoryMapper.addAvailable(student.getDormitoryId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Student> moveoutList() {
        // 查询所有已入住的学生信息
        return this.studentMapper.moveoutList();
    }

    @Override
    public List<Student> searchForMoveoutList(String key, String value) {
        // 如果value为空，则返回全部已入住的学生信息
        if(value.equals("")) return this.studentMapper.moveoutList();
        List<Student> list = null;
        switch (key){
            case "number":
                list = this.studentMapper.searchForMoveoutByNumber(value);
                break;
            case "name":
                list = this.studentMapper.searchForMoveoutByName(value);
                break;
        }
        return list;
    }

    @Override
    public void moveout(Moveout moveout) {
        try {
            // 将该学生所属的宿舍的可用床位数加一
            this.dormitoryMapper.addAvailable(moveout.getDormitoryId());
            // 将该学生的状态设置为“迁出”
            this.studentMapper.updateStateById(moveout.getStudentId());
            // 获取当前时间，并将其格式化为“yyyy-MM-dd”形式
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            moveout.setCreateDate(format.format(date));
            // 将该学生的迁出信息插入到数据库中
            this.studentMapper.moveout(moveout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Student> findByDormitoryId(Integer dormitoryId) {
        // 根据宿舍id查询该宿舍内的所有学生信息
        return this.studentMapper.findByDormitoryId(dormitoryId);
    }
}
