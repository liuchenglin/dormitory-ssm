package com.southwind.controller;

import com.southwind.entity.Building;
import com.southwind.service.BuildingService;
import com.southwind.service.DormitoryAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/building")
public class BuildingController {

    @Autowired
    private BuildingService buildingService;
    @Autowired
    private DormitoryAdminService dormitoryAdminService;
    /**
     * 获取所有楼栋信息
     *
     * @return ModelAndView对象，包含楼栋列表和相关的宿舍管理员列表的视图
     */
    @GetMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("buildingmanager");
        modelAndView.addObject("list", this.buildingService.list());
        modelAndView.addObject("dormitoryAdminList", this.dormitoryAdminService.list());
        return modelAndView;
    }
    /**
     * 根据关键字搜索楼栋信息
     *
     * @param key   搜索关键字（字段名）
     * @param value 搜索关键字（字段值）
     * @return ModelAndView对象，包含搜索结果和相关的宿舍管理员列表的视图
     */
    @PostMapping("/search")
    public ModelAndView search(String key,String value){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("buildingmanager");
        modelAndView.addObject("list", this.buildingService.search(key, value));
        modelAndView.addObject("dormitoryAdminList", this.dormitoryAdminService.list());
        return modelAndView;
    }
    /**
     * 保存楼栋信息
     *
     * @param building 待保存的楼栋对象
     * @return 重定向到楼栋列表页面
     */
    @PostMapping("/save")
    public String save(Building building){
        this.buildingService.save(building);
        return "redirect:/building/list";
    }
    /**
     * 更新楼栋信息
     *
     * @param building 待更新的楼栋对象
     * @return 重定向到楼栋列表页面
     */
    @PostMapping("/update")
    public String update(Building building){
        this.buildingService.update(building);
        return "redirect:/building/list";
    }
    /**
     * 删除楼栋信息
     *
     * @param id 待删除楼栋的ID
     * @return 重定向到楼栋列表页面
     */
    @PostMapping("/delete")
    public String delete(Integer id){
        this.buildingService.delete(id);
        return "redirect:/building/list";
    }
}
